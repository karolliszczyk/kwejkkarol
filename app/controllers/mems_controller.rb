class MemsController < ApplicationController
  before_action :set_mem, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user! , only: [:new,:show,:edit,:update,:my]

  # GET /mems
  # GET /mems.json
  def index
    @mems = Mem.where(active: true)
  end

  def inactive
    @mems = Mem.where(active: false)
    render :index
  end


  # GET /mems/1
  # GET /mems/1.json
  def show
  end

  # GET /mems/new
  def new
    @mem = Mem.new
  end

  def my
    @mems = current_user.mems
    render :index
  end


  # GET /mems/1/edit
  def edit
  end

  # POST /mems
  # POST /mems.json
  def create
    @mem = current_user.mems.new(mem_params)
    @mem.image = params[:mem][:image]
    @mem.active = false
    respond_to do |format|
      if @mem.save
        format.html { redirect_to @mem, notice: 'Mem was successfully created.' }
        format.json { render action: 'show', status: :created, location: @mem }
      else
        format.html { render action: 'new' }
        format.json { render json: @mem.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /mems/1
  # PATCH/PUT /mems/1.json
  def update
    respond_to do |format|
      if @mem.update(mem_params)
        format.html { redirect_to @mem, notice: 'Mem was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @mem.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mems/1
  # DELETE /mems/1.json
  def destroy
    @mem.destroy
    respond_to do |format|
      format.html { redirect_to mems_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_mem
      @mem = Mem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def mem_params
      params.require(:mem).permit(:name, :description, :user_id)
    end
end
